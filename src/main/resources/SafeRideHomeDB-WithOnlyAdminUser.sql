/*
SQLyog Community v11.31 (64 bit)
MySQL - 5.6.15 : Database - saferidehome
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`saferidehome` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `saferidehome`;

/*Table structure for table `ride` */

DROP TABLE IF EXISTS `ride`;

CREATE TABLE `ride` (
  `rideId` varchar(25) NOT NULL COMMENT 'Unique Number for a ride',
  `fromAdd` varchar(45) DEFAULT NULL COMMENT 'Drop down Address requested by the user',
  `toAdd` varchar(45) DEFAULT NULL COMMENT 'Pick Up Address requested by the user ',
  `noOfRiders` int(2) DEFAULT NULL COMMENT 'No of People to be served for that ride',
  `status` enum('Open','Assigned','PickedUp','DroppedOff','Cancelled','NoAnswer') DEFAULT NULL,
  `scheduledBy` varchar(45) DEFAULT NULL,
  `vehicle` varchar(45) DEFAULT NULL,
  `coRiders` varchar(200) DEFAULT NULL,
  `riderComments` varchar(500) DEFAULT NULL,
  `driverComments` varchar(500) DEFAULT NULL,
  `driver` varchar(45) DEFAULT NULL COMMENT 'driver who served the ride',
  `requestedDate` date DEFAULT NULL,
  `requestedTime` time DEFAULT NULL,
  `assignedDate` date DEFAULT NULL,
  `assignedTime` time DEFAULT NULL,
  `pickedUpDate` date DEFAULT NULL,
  `pickedUpTime` time DEFAULT NULL,
  `droppedOffDate` date DEFAULT NULL,
  `droppedOffTime` time DEFAULT NULL,
  `cancelledDate` date DEFAULT NULL,
  `cancelledTime` time DEFAULT NULL,
  `assignedBy` varchar(45) DEFAULT NULL,
  `cancelledBy` varchar(45) DEFAULT NULL,
  `callBackNumber` varchar(45) DEFAULT NULL,
  `requestedBy` varchar(45) DEFAULT NULL,
  `noOfNWStudents` int(11) DEFAULT NULL,
  `noOfFemaleRiders` int(11) DEFAULT NULL,
  `noOfMaleRiders` int(11) DEFAULT NULL,
  `noOfOtherRiders` int(11) DEFAULT NULL,
  `noOfNonNwStudents` int(11) DEFAULT NULL,
  `riderName` varchar(100) DEFAULT NULL,
  `noAnswerDate` date DEFAULT NULL,
  `noAnswerTime` time DEFAULT NULL,
  PRIMARY KEY (`rideId`),
  KEY `fk_ride_user_idx` (`scheduledBy`),
  KEY `fk_ride_service1_idx` (`vehicle`),
  KEY `fk_ride_user1_idx` (`driver`),
  KEY `fk_ride_user2_idx` (`assignedBy`),
  KEY `fk_ride_user3_idx` (`cancelledBy`),
  CONSTRAINT `fk_ride_service1` FOREIGN KEY (`vehicle`) REFERENCES `vehicle` (`vehicleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ride_user` FOREIGN KEY (`scheduledBy`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ride_user1` FOREIGN KEY (`driver`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ride_user2` FOREIGN KEY (`assignedBy`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ride_user3` FOREIGN KEY (`cancelledBy`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ride` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `userId` varchar(45) NOT NULL COMMENT '919# number of the student.',
  `password` varchar(45) DEFAULT 'default' COMMENT 'Password of the user',
  `userType` varchar(45) DEFAULT NULL COMMENT 'Type may be student, faculty or staff',
  `name` varchar(45) DEFAULT NULL COMMENT 'firstName of the user',
  `emailId` varchar(45) DEFAULT NULL COMMENT 'Mail ID of the user',
  `phone` varchar(45) DEFAULT NULL COMMENT 'Phone number of the user',
  `accountStatus` enum('Registered','Active','Blocked') DEFAULT NULL,
  `verificationCode` varchar(45) DEFAULT 'default' COMMENT 'verification code of the user',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`userId`,`password`,`userType`,`name`,`emailId`,`phone`,`accountStatus`,`verificationCode`) values ('admin','UPD','ADMIN','Monica','mkahman@nwmissouri.edu','6605621254','Active','default');

/*Table structure for table `vehicle` */

DROP TABLE IF EXISTS `vehicle`;

CREATE TABLE `vehicle` (
  `vehicleId` varchar(45) NOT NULL COMMENT 'Unique ID given to the service',
  `description` varchar(45) DEFAULT NULL COMMENT 'Description of the service',
  `capacity` int(11) DEFAULT NULL COMMENT 'Maximum Capacity of the Service ',
  `phone` varchar(10) DEFAULT NULL,
  `status` enum('Available','Suspended') DEFAULT NULL COMMENT 'Unavailable, Available, InUse',
  PRIMARY KEY (`vehicleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vehicle` */

insert  into `vehicle`(`vehicleId`,`description`,`capacity`,`phone`,`status`) values ('110','minivan',6,'6605624447','Available'),('19-7','minivan',4,'6605624446','Available'),('19-9','minivan',6,'6602544449','Available');

/*Table structure for table `vehiclelocation` */

DROP TABLE IF EXISTS `vehiclelocation`;

CREATE TABLE `vehiclelocation` (
  `vehicleId` varchar(20) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vehiclelocation` */

/* Procedure structure for procedure `fn` */

/*!50003 DROP PROCEDURE IF EXISTS  `fn` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `fn`(cid INT)
BEGIN

  SELECT * FROM ride;

END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
