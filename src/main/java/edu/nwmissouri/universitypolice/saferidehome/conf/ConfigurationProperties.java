package edu.nwmissouri.universitypolice.saferidehome.conf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;

public class ConfigurationProperties {

	public static String dbHost = "localhost";
	public static String dbSchema = "saferidehome";
	public static String dbPort = "3306";
	public static String dbUsername = "root";
	public static String dbPassword = "root";

	public static String host = "localhost";
	public static String port = "8585";

	public static String emailId = "nwmsusaferidehome@gmail.com";
	public static String emailPassword = "universitypolice";

	static {
		try {
			URL whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(
						whatismyip.openStream()));
				host = in.readLine();
			} catch (Exception e) {
				host = (InetAddress.getLocalHost().getHostAddress()).toString();
			} finally {
				try {
					in.close();
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
		}
	}

}
