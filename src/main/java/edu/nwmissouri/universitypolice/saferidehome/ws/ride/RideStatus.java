package edu.nwmissouri.universitypolice.saferidehome.ws.ride;

import edu.nwmissouri.universitypolice.saferidehome.pojos.Ride;

import java.io.Serializable;

public class RideStatus implements Serializable {

	private String eta;
	private String noOfStops;
	private String ride;
	

	public RideStatus() {
		super();
	}

	public RideStatus(String eta, String noOfStops, String ride) {
		super();
		this.eta = eta;
		this.noOfStops = noOfStops;
		this.ride = ride;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(String noOfStops) {
		this.noOfStops = noOfStops;
	}

}
