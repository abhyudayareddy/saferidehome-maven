package edu.nwmissouri.universitypolice.saferidehome.ws.shuttle;

import com.google.gson.Gson;
import edu.nwmissouri.universitypolice.saferidehome.pojos.User;
import edu.nwmissouri.universitypolice.saferidehome.pojos.VehicleLocation;
import edu.nwmissouri.universitypolice.saferidehome.service.SafeRideHomeService;
import edu.nwmissouri.universitypolice.saferidehome.ws.login.LoginResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/shuttle")
public class ShuttleLocateService {

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/readCoordinates/{vehicleId}")
    public String getVehicleLocation(@PathParam("vehicleId") String vehicleId) {
        SafeRideHomeService util;
        return getLocation(vehicleId);
    }

    public String getLocation(String vehicleId) {
        SafeRideHomeService util = new SafeRideHomeService();
        LoginResponse response = null;
        Gson gson;
        try {
            VehicleLocation location = util.getVehicleLocation(vehicleId);
            if (location != null) {
                gson = new Gson();
                return gson.toJson(location);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return "ERROR";
    }


    @Path("/writeCoordinates/{vehicleId}/{latitude}/{longitude}")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String verifyUser(@PathParam("vehicleId") String vehicleId,
                             @PathParam("latitude") String latitude, @PathParam("longitude") String longitude) {
        return updateVehicleLocation(vehicleId, latitude, longitude);
    }

    public String updateVehicleLocation(String vehicleId, String latitude, String longitude) {
        try {
            SafeRideHomeService util = new SafeRideHomeService();
            if (util.updateVehicleLocation(vehicleId, latitude, longitude)) {
                return "SUCCESS";
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return "FAILURE";
    }

}
