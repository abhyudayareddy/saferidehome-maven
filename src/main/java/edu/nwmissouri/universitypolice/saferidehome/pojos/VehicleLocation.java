package edu.nwmissouri.universitypolice.saferidehome.pojos;

import java.io.Serializable;

/**
 * Created by Abhyuday Reddy on 6/5/2014.
 */
public class VehicleLocation implements Serializable {
    private String vehicleId;
    private String latitude;
    private String longitude;

    public VehicleLocation(String vehicleId, String latitude, String longitude) {
        this.vehicleId = vehicleId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public VehicleLocation() {
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
